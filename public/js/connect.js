/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./development/components/js/connect.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/openMyBurger */ "./development/components/js/modules/openMyBurger.js");
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _modules_AddGeneratorTable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/AddGeneratorTable */ "./development/components/js/modules/AddGeneratorTable.js");
/* harmony import */ var _modules_AddGeneratorTable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_modules_AddGeneratorTable__WEBPACK_IMPORTED_MODULE_2__);
// ============================
//    Name: index.js
// ============================




var start = function start() {
  console.log('DOM:', 'DOMContentLoaded', true);
  new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default.a({
    selector: '.js__moveScrollUP',
    speed: 8
  }).run();
  new _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1___default.a({
    burger: '.js__navHamburger',
    navbar: '.js__navHamburgerOpener'
  }).run();
  new _modules_AddGeneratorTable__WEBPACK_IMPORTED_MODULE_2___default.a({
    selector: '.js__selectorTable',
    server: 'server32.json',
    serverBig: 'server1000.json'
  }).run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  document.addEventListener('DOMContentLoaded', start(), false);
}

/***/ }),

/***/ "./development/components/js/modules/AddGeneratorTable.js":
/*!****************************************************************!*\
  !*** ./development/components/js/modules/AddGeneratorTable.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddGeneratorTable(init) {
    _classCallCheck(this, AddGeneratorTable);

    this.selector = init.selector;
    this.server = init.server;
    this.serverBig = init.serverBig;

    this.sortUp = function (a, b) {
      return b < a ? -1 : b > a ? 1 : b >= a ? 0 : NaN;
    };

    this.sortDown = function (a, b) {
      return a < b ? -1 : a > b ? 1 : a >= b ? 0 : NaN;
    };

    this.mydata = '';
    this.tableLine = '';
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddGeneratorTable, [{
    key: "run",
    value: function run() {
      var _this2 = this;

      var selector = document.querySelector("".concat(this.selector));

      if (selector) {
        this.constructor.info();
        var init = {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          },
          mode: 'cors',
          cache: 'default'
        };
        fetch("./".concat(this.server), init).then(function (response) {
          response.headers.get('Content-Type');
          return response.json();
        }).then(function (data) {
          selector.innerHTML = '';
          var createElemUX = document.createElement('DIV');
          createElemUX.classList.add('myformcontent__header');
          _this2.mydata = Object.keys(data[0]).filter(function (item, index, array) {
            return index < array.length - 2;
          });

          _this2.mydata.map(function (headerName) {
            createElemUX.innerHTML += "\n\t\t\t\t\t\t<div class=\"myformcontent__header-item\">\n\t\t\t\t\t\t\t<span class=\"myformcontent__header-text\">".concat(headerName, "</span>\n\t\t\t\t\t\t\t<span class=\"myformcontent__arrow--down\">\n\t\t\t\t\t\t\t\t<svg role=\"picture-svg\" class=\"glyphs__caret-down\"><use xlink:href=\"#id-glyphs-caret-down\"></use></svg>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t");
            return selector.appendChild(createElemUX);
          });

          _this2.tableLine = Object.values(data.slice(0, 50));

          var tableLineSortID = _this2.tableLine.sort(function (a, b) {
            return _this2.sortDown(a[_this2.mydata[1]], b[_this2.mydata[1]]);
          });

          tableLineSortID.forEach(function (f) {
            var createElemLine = document.createElement('DIV');
            createElemLine.className = 'myformcontent__line';
            var tableLineItem = Object.values(f).filter(function (item, index, array) {
              return index < array.length - 2;
            });
            tableLineItem.forEach(function (item) {
              var createElemLineItem = document.createElement('DIV');
              createElemLineItem.className = 'myformcontent__line-item';
              createElemLineItem.innerHTML += item;
              createElemLine.appendChild(createElemLineItem);
              return false;
            });
            selector.appendChild(createElemLine);
          });
        }).then(function () {
          var clickHeader = function clickHeader(event) {
            var _this = event.target;

            var index = _toConsumableArray(_this.parentElement.children).indexOf(_this);

            if (_this.classList.contains('myformcontent__header')) return;

            if (_this.parentElement.querySelector('.active')) {
              _this.parentElement.querySelector('.active').classList.remove('active');
            }

            _this2.tableLine.sort(function (a, b) {
              return _this2.sortDown(a[_this2.mydata[index]], b[_this2.mydata[index]]);
            }).reverse();

            console.log('this.tableLine', _this2.tableLine);

            _this2.tableLine.forEach(function (f, indexArray) {
              console.log('sort:', f.id);
              var numb = String(f.id);
              console.log('numb', numb, _typeof(numb));

              var _temp = _toConsumableArray(document.querySelectorAll('.myformcontent__line')).filter(function (item) {
                return item.firstChild.textContent === numb;
              });

              console.log('_temp', _temp[0], _typeof(_temp[0]));
              document.querySelector('.myformcontent__header').after(_temp[0]);
            });

            _this.parentElement.children[index].classList.add('active');
          };

          selector.querySelector('.myformcontent__header').firstElementChild.classList.add('active');
          selector.querySelector('.myformcontent__header').addEventListener('click', clickHeader);
        }) // const clickHeader = (event) => {
        // 	console.log(event.target);
        // 	[...document.querySelectorAll('.myformcontent__header-item')].forEach(f => f.classList.remove('active'));
        // 	if (event.target.matches('.myformcontent__header-item')) {
        // 		event.target.classList.add('active');
        // 	}
        // 	selector.querySelector('.myformcontent__header-item').classList.remove('active');
        // };
        // const myRemove = [this.sortCar, this.sortHeader];
        // myRemove.forEach(el => (el.textContent.includes(this.errorText) ? el.remove() : ''));
        // Object.values(data[0].column).map((i, index) => {
        // 	const creatDIV = document.createElement('DIV');
        // 	creatDIV.className = 'tools__sort-header';
        // 	creatDIV.innerHTML += i.title;
        // 	selector.querySelector('.tools__sort-title').appendChild(creatDIV);
        // 	return false;
        // });
        // Object.values(data.slice(1)).forEach((i, index) => {
        // 	const createElemUX = document.createElement('DIV');
        // 	createElemUX.className = 'tools__sort-car tools__sort-line';
        // 	Object.values(i.car).forEach((f, fIndex) => {
        // 		const createElemDIV = document.createElement('DIV');
        // 		createElemDIV.className = 'tools__sort-item';
        // 		createElemDIV.innerHTML += f;
        // 		createElemUX.appendChild(createElemDIV);
        // 		return false;
        // 	});
        // 	selector.appendChild(createElemUX);
        // });
        // .then(() => {
        // 	const droplist = document.querySelector('.js__createDropList');
        // 	const selectUl = document.createElement('UL');
        // 	selectUl.className = 'dropdown__item dropdown__select';
        // 	[...document.querySelectorAll('.tools__sort-car')].forEach((f, index) => {
        // 		const selectLi = document.createElement('LI');
        // 		selectUl.appendChild(selectLi);
        // 		selectLi.innerHTML += f.children[0].textContent;
        // 	});
        // 	droplist.appendChild(selectUl);
        // })
        // .then(() => {
        // 	const element = document.querySelector('MAIN');
        // 	element.addEventListener('click', (event) => {
        // 		document.querySelector('.js__createDropList UL').classList.remove('open');
        // 		if (event.target.matches('.dropdown__label')) {
        // 			event.preventDefault() && event.stopPropagation();
        // 		}
        // 		if (event.target.matches('.js__createDropList INPUT')) {
        // 			event.target.parentElement.querySelector('UL').classList.add('open');
        // 		}
        // 		if (event.target.nodeName === 'LI') {
        // 			const grabText = event.target.textContent;
        // 			const pasteInSelector = event.target.parentElement.parentElement;
        // 			pasteInSelector.querySelector('INPUT').value = grabText || '';
        // 		}
        // 	});
        // })
        .catch(function (error) {
          return console.log("".concat(_this2.errorText, ": \n --> ").concat(error.message));
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddGeneratorTable;
}();

/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddMoveScrollUP(init) {
    _classCallCheck(this, AddMoveScrollUP);

    this.selector = init.selector;
    this.count = Number(Math.abs(init.speed)) || 8;
    this.speed = this.count <= 20 ? this.count : 8;
    this.myPos = window.pageYOffset;
    this.getScroll = document.documentElement.scrollTop;
    this.speedScroll = this.speed;
  }

  _createClass(AddMoveScrollUP, [{
    key: "run",
    value: function run() {
      var _this = this;

      var selector = document.querySelector("".concat(this.selector));

      if (selector) {
        this.constructor.info();

        var clickedArrow = function clickedArrow() {
          _this.getScroll = document.documentElement.scrollTop;

          if (_this.getScroll >= 1) {
            window.requestAnimationFrame(clickedArrow);
            window.scrollTo(0, _this.getScroll - _this.getScroll / _this.speedScroll);
          }
        };

        var scrolledDown = function scrolledDown() {
          _this.myPos = window.pageYOffset;
          _this.myPos >= 100 ? selector.classList.add('open') : selector.classList.remove('open');
        };

        selector.addEventListener('click', clickedArrow);
        window.addEventListener('scroll', scrolledDown);
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddMoveScrollUP;
}();

/***/ }),

/***/ "./development/components/js/modules/openMyBurger.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/openMyBurger.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddOpenMyBurger(init) {
    _classCallCheck(this, AddOpenMyBurger);

    this.selector = init.burger;
    this.navbar = init.navbar;
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddOpenMyBurger, [{
    key: "run",
    value: function run() {
      var _this = this;

      var selector = document.querySelector("".concat(this.selector));

      if (selector) {
        this.constructor.info();

        var openBurger = function openBurger() {
          var nav = document.querySelector("".concat(_this.navbar));
          nav.querySelector('DIV').classList.toggle('open');
          selector.classList.toggle('open');
        };

        selector.addEventListener('click', function () {
          return openBurger();
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddOpenMyBurger;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL0FkZEdlbmVyYXRvclRhYmxlLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9tb3ZlU2Nyb2xsVVAuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL29wZW5NeUJ1cmdlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW5kZXguanNcclxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuaW1wb3J0IEFkZE1vdmVTY3JvbGxVUCBmcm9tICcuL21vZHVsZXMvbW92ZVNjcm9sbFVQJztcclxuaW1wb3J0IEFkZE9wZW5NeUJ1cmdlciBmcm9tICcuL21vZHVsZXMvb3Blbk15QnVyZ2VyJztcclxuaW1wb3J0IEFkZEdlbmVyYXRvclRhYmxlIGZyb20gJy4vbW9kdWxlcy9BZGRHZW5lcmF0b3JUYWJsZSc7XHJcblxyXG5jb25zdCBzdGFydCA9ICgpID0+IHtcclxuXHRjb25zb2xlLmxvZygnRE9NOicsICdET01Db250ZW50TG9hZGVkJywgdHJ1ZSk7XHJcblxyXG5cdG5ldyBBZGRNb3ZlU2Nyb2xsVVAoe1xyXG5cdFx0c2VsZWN0b3I6ICcuanNfX21vdmVTY3JvbGxVUCcsXHJcblx0XHRzcGVlZDogOFxyXG5cdH0pLnJ1bigpO1xyXG5cdFxyXG5cdG5ldyBBZGRPcGVuTXlCdXJnZXIoe1xyXG5cdFx0YnVyZ2VyOiAnLmpzX19uYXZIYW1idXJnZXInLFxyXG5cdFx0bmF2YmFyOiAnLmpzX19uYXZIYW1idXJnZXJPcGVuZXInXHJcblx0fSkucnVuKCk7XHJcblx0XHJcblx0bmV3IEFkZEdlbmVyYXRvclRhYmxlKHtcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19zZWxlY3RvclRhYmxlJyxcclxuXHRcdHNlcnZlcjogJ3NlcnZlcjMyLmpzb24nLFxyXG5cdFx0c2VydmVyQmlnOiAnc2VydmVyMTAwMC5qc29uJ1xyXG5cdH0pLnJ1bigpO1xyXG59O1xyXG5cclxuaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdyAmJiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcikge1xyXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBzdGFydCgpLCBmYWxzZSk7XHJcbn1cclxuICIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkR2VuZXJhdG9yVGFibGUge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBpbml0LnNlbGVjdG9yO1xyXG5cclxuXHRcdHRoaXMuc2VydmVyID0gaW5pdC5zZXJ2ZXI7XHJcblx0XHR0aGlzLnNlcnZlckJpZyA9IGluaXQuc2VydmVyQmlnO1xyXG5cclxuXHRcdHRoaXMuc29ydFVwID0gKGEsIGIpID0+IChiIDwgYSkgPyAtMSA6IChiID4gYSkgPyAxIDogKGIgPj0gYSkgPyAwIDogTmFOO1xyXG5cdFx0dGhpcy5zb3J0RG93biA9IChhLCBiKSA9PiAoYSA8IGIpID8gLTEgOiAoYSA+IGIpID8gMSA6IChhID49IGIpID8gMCA6IE5hTjtcclxuXHJcblx0XHR0aGlzLm15ZGF0YSA9ICcnO1xyXG5cdFx0dGhpcy50YWJsZUxpbmUgPSAnJztcclxuXHJcblx0XHR0aGlzLmVycm9yVGV4dCA9ICfwn5KAIE91Y2gsIGRhdGFiYXNlIGVycm9yLi4uJztcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3Qgc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuc2VsZWN0b3J9YCk7XHJcblx0XHRpZiAoc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblxyXG5cdFx0XHRjb25zdCBpbml0ID0ge1xyXG5cdFx0XHRcdG1ldGhvZDogJ0dFVCcsXHJcblx0XHRcdFx0aGVhZGVyczogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH0sXHJcblx0XHRcdFx0bW9kZTogJ2NvcnMnLFxyXG5cdFx0XHRcdGNhY2hlOiAnZGVmYXVsdCdcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGZldGNoKGAuLyR7dGhpcy5zZXJ2ZXJ9YCwgaW5pdClcclxuXHRcdFx0XHQudGhlbigocmVzcG9uc2UpID0+IHtcclxuXHRcdFx0XHRcdHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKTtcclxuXHRcdFx0XHRcdHJldHVybiByZXNwb25zZS5qc29uKCk7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQudGhlbigoZGF0YSkgPT4ge1xyXG5cdFx0XHRcdFx0c2VsZWN0b3IuaW5uZXJIVE1MID0gJyc7XHJcblxyXG5cdFx0XHRcdFx0Y29uc3QgY3JlYXRlRWxlbVVYID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnRElWJyk7XHJcblx0XHRcdFx0XHRjcmVhdGVFbGVtVVguY2xhc3NMaXN0LmFkZCgnbXlmb3JtY29udGVudF9faGVhZGVyJyk7XHJcblxyXG5cdFx0XHRcdFx0dGhpcy5teWRhdGEgPSBPYmplY3Qua2V5cyhkYXRhWzBdKS5maWx0ZXIoKGl0ZW0sIGluZGV4LCBhcnJheSkgPT4gaW5kZXggPCBhcnJheS5sZW5ndGggLSAyKTtcclxuXHJcblx0XHRcdFx0XHR0aGlzLm15ZGF0YS5tYXAoKGhlYWRlck5hbWUpID0+IHtcclxuXHRcdFx0XHRcdFx0Y3JlYXRlRWxlbVVYLmlubmVySFRNTCArPSBgXHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJteWZvcm1jb250ZW50X19oZWFkZXItaXRlbVwiPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibXlmb3JtY29udGVudF9faGVhZGVyLXRleHRcIj4ke2hlYWRlck5hbWV9PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuIGNsYXNzPVwibXlmb3JtY29udGVudF9fYXJyb3ctLWRvd25cIj5cclxuXHRcdFx0XHRcdFx0XHRcdDxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2NhcmV0LWRvd25cIj48dXNlIHhsaW5rOmhyZWY9XCIjaWQtZ2x5cGhzLWNhcmV0LWRvd25cIj48L3VzZT48L3N2Zz5cclxuXHRcdFx0XHRcdFx0XHQ8L3NwYW4+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0YDtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIHNlbGVjdG9yLmFwcGVuZENoaWxkKGNyZWF0ZUVsZW1VWCk7XHJcblx0XHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0XHR0aGlzLnRhYmxlTGluZSA9IE9iamVjdC52YWx1ZXMoZGF0YS5zbGljZSgwLCA1MCkpO1xyXG5cdFx0XHRcdFx0Y29uc3QgdGFibGVMaW5lU29ydElEID0gdGhpcy50YWJsZUxpbmUuc29ydCgoYSwgYikgPT4gdGhpcy5zb3J0RG93bihhW3RoaXMubXlkYXRhWzFdXSwgYlt0aGlzLm15ZGF0YVsxXV0pKTtcclxuXHJcblx0XHRcdFx0XHR0YWJsZUxpbmVTb3J0SUQuZm9yRWFjaCgoZikgPT4ge1xyXG5cdFx0XHRcdFx0XHRjb25zdCBjcmVhdGVFbGVtTGluZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0XHRcdFx0XHRjcmVhdGVFbGVtTGluZS5jbGFzc05hbWUgPSAnbXlmb3JtY29udGVudF9fbGluZSc7XHJcblxyXG5cdFx0XHRcdFx0XHRjb25zdCB0YWJsZUxpbmVJdGVtID0gT2JqZWN0LnZhbHVlcyhmKS5maWx0ZXIoKGl0ZW0sIGluZGV4LCBhcnJheSkgPT4gaW5kZXggPCBhcnJheS5sZW5ndGggLSAyKTtcclxuXHJcblx0XHRcdFx0XHRcdHRhYmxlTGluZUl0ZW0uZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdGNvbnN0IGNyZWF0ZUVsZW1MaW5lSXRlbSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0XHRcdFx0XHRcdGNyZWF0ZUVsZW1MaW5lSXRlbS5jbGFzc05hbWUgPSAnbXlmb3JtY29udGVudF9fbGluZS1pdGVtJztcclxuXHRcdFx0XHRcdFx0XHRjcmVhdGVFbGVtTGluZUl0ZW0uaW5uZXJIVE1MICs9IGl0ZW07XHJcblx0XHRcdFx0XHRcdFx0Y3JlYXRlRWxlbUxpbmUuYXBwZW5kQ2hpbGQoY3JlYXRlRWxlbUxpbmVJdGVtKTtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRcdFx0c2VsZWN0b3IuYXBwZW5kQ2hpbGQoY3JlYXRlRWxlbUxpbmUpO1xyXG5cdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LnRoZW4oKCkgPT4ge1xyXG5cdFx0XHRcdFx0Y29uc3QgY2xpY2tIZWFkZXIgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRcdFx0Y29uc3QgX3RoaXMgPSBldmVudC50YXJnZXQ7XHJcblx0XHRcdFx0XHRcdGNvbnN0IGluZGV4ID0gWy4uLl90aGlzLnBhcmVudEVsZW1lbnQuY2hpbGRyZW5dLmluZGV4T2YoX3RoaXMpO1xyXG5cclxuXHRcdFx0XHRcdFx0aWYgKF90aGlzLmNsYXNzTGlzdC5jb250YWlucygnbXlmb3JtY29udGVudF9faGVhZGVyJykpIHJldHVybjtcclxuXHRcdFx0XHRcdFx0aWYgKF90aGlzLnBhcmVudEVsZW1lbnQucXVlcnlTZWxlY3RvcignLmFjdGl2ZScpKSB7XHJcblx0XHRcdFx0XHRcdFx0X3RoaXMucGFyZW50RWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlJykuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdHRoaXMudGFibGVMaW5lLnNvcnQoKGEsIGIpID0+IHRoaXMuc29ydERvd24oYVt0aGlzLm15ZGF0YVtpbmRleF1dLCBiW3RoaXMubXlkYXRhW2luZGV4XV0pKS5yZXZlcnNlKCk7XHJcblx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKCd0aGlzLnRhYmxlTGluZScsIHRoaXMudGFibGVMaW5lKTtcclxuXHJcblx0XHRcdFx0XHRcdHRoaXMudGFibGVMaW5lLmZvckVhY2goKGYsIGluZGV4QXJyYXkpID0+IHtcclxuXHJcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ3NvcnQ6JywgZi5pZCk7XHJcblx0XHRcdFx0XHRcdFx0Y29uc3QgbnVtYiA9IFN0cmluZyhmLmlkKTtcclxuXHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZygnbnVtYicsIG51bWIsIHR5cGVvZiBudW1iKTtcclxuXHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0Y29uc3QgX3RlbXAgPSBbLi4uZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm15Zm9ybWNvbnRlbnRfX2xpbmUnKV0uZmlsdGVyKGl0ZW0gPT4gaXRlbS5maXJzdENoaWxkLnRleHRDb250ZW50ID09PSBudW1iKTtcclxuXHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZygnX3RlbXAnLCBfdGVtcFswXSwgdHlwZW9mIF90ZW1wWzBdKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm15Zm9ybWNvbnRlbnRfX2hlYWRlcicpLmFmdGVyKF90ZW1wWzBdKTtcclxuXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRcdFx0X3RoaXMucGFyZW50RWxlbWVudC5jaGlsZHJlbltpbmRleF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHR9O1xyXG5cdFx0XHRcdFx0c2VsZWN0b3IucXVlcnlTZWxlY3RvcignLm15Zm9ybWNvbnRlbnRfX2hlYWRlcicpLmZpcnN0RWxlbWVudENoaWxkLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0c2VsZWN0b3IucXVlcnlTZWxlY3RvcignLm15Zm9ybWNvbnRlbnRfX2hlYWRlcicpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tIZWFkZXIpO1xyXG5cclxuXHRcdFx0XHR9KVxyXG5cclxuXHRcdFx0XHQvLyBjb25zdCBjbGlja0hlYWRlciA9IChldmVudCkgPT4ge1xyXG5cdFx0XHRcdC8vIFx0Y29uc29sZS5sb2coZXZlbnQudGFyZ2V0KTtcclxuXHRcdFx0XHQvLyBcdFsuLi5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubXlmb3JtY29udGVudF9faGVhZGVyLWl0ZW0nKV0uZm9yRWFjaChmID0+IGYuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJykpO1xyXG5cdFx0XHRcdC8vIFx0aWYgKGV2ZW50LnRhcmdldC5tYXRjaGVzKCcubXlmb3JtY29udGVudF9faGVhZGVyLWl0ZW0nKSkge1xyXG5cdFx0XHRcdC8vIFx0XHRldmVudC50YXJnZXQuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcblx0XHRcdFx0Ly8gXHR9XHJcblxyXG5cdFx0XHRcdC8vIFx0c2VsZWN0b3IucXVlcnlTZWxlY3RvcignLm15Zm9ybWNvbnRlbnRfX2hlYWRlci1pdGVtJykuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcblx0XHRcdFx0Ly8gfTtcclxuXHJcblx0XHRcdFx0Ly8gY29uc3QgbXlSZW1vdmUgPSBbdGhpcy5zb3J0Q2FyLCB0aGlzLnNvcnRIZWFkZXJdO1xyXG5cdFx0XHRcdC8vIG15UmVtb3ZlLmZvckVhY2goZWwgPT4gKGVsLnRleHRDb250ZW50LmluY2x1ZGVzKHRoaXMuZXJyb3JUZXh0KSA/IGVsLnJlbW92ZSgpIDogJycpKTtcclxuXHJcblx0XHRcdFx0Ly8gT2JqZWN0LnZhbHVlcyhkYXRhWzBdLmNvbHVtbikubWFwKChpLCBpbmRleCkgPT4ge1xyXG5cdFx0XHRcdC8vIFx0Y29uc3QgY3JlYXRESVYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdESVYnKTtcclxuXHRcdFx0XHQvLyBcdGNyZWF0RElWLmNsYXNzTmFtZSA9ICd0b29sc19fc29ydC1oZWFkZXInO1xyXG5cdFx0XHRcdC8vIFx0Y3JlYXRESVYuaW5uZXJIVE1MICs9IGkudGl0bGU7XHJcblx0XHRcdFx0Ly8gXHRzZWxlY3Rvci5xdWVyeVNlbGVjdG9yKCcudG9vbHNfX3NvcnQtdGl0bGUnKS5hcHBlbmRDaGlsZChjcmVhdERJVik7XHJcblx0XHRcdFx0Ly8gXHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0Ly8gfSk7XHJcblxyXG5cdFx0XHRcdC8vIE9iamVjdC52YWx1ZXMoZGF0YS5zbGljZSgxKSkuZm9yRWFjaCgoaSwgaW5kZXgpID0+IHtcclxuXHRcdFx0XHQvLyBcdGNvbnN0IGNyZWF0ZUVsZW1VWCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0XHRcdC8vIFx0Y3JlYXRlRWxlbVVYLmNsYXNzTmFtZSA9ICd0b29sc19fc29ydC1jYXIgdG9vbHNfX3NvcnQtbGluZSc7XHJcblxyXG5cdFx0XHRcdC8vIFx0T2JqZWN0LnZhbHVlcyhpLmNhcikuZm9yRWFjaCgoZiwgZkluZGV4KSA9PiB7XHJcblx0XHRcdFx0Ly8gXHRcdGNvbnN0IGNyZWF0ZUVsZW1ESVYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdESVYnKTtcclxuXHRcdFx0XHQvLyBcdFx0Y3JlYXRlRWxlbURJVi5jbGFzc05hbWUgPSAndG9vbHNfX3NvcnQtaXRlbSc7XHJcblx0XHRcdFx0Ly8gXHRcdGNyZWF0ZUVsZW1ESVYuaW5uZXJIVE1MICs9IGY7XHJcblx0XHRcdFx0Ly8gXHRcdGNyZWF0ZUVsZW1VWC5hcHBlbmRDaGlsZChjcmVhdGVFbGVtRElWKTtcclxuXHRcdFx0XHQvLyBcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0XHRcdC8vIFx0fSk7XHJcblxyXG5cdFx0XHRcdC8vIFx0c2VsZWN0b3IuYXBwZW5kQ2hpbGQoY3JlYXRlRWxlbVVYKTtcclxuXHRcdFx0XHQvLyB9KTtcclxuXHJcblx0XHRcdFx0Ly8gLnRoZW4oKCkgPT4ge1xyXG5cdFx0XHRcdC8vIFx0Y29uc3QgZHJvcGxpc3QgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuanNfX2NyZWF0ZURyb3BMaXN0Jyk7XHJcblx0XHRcdFx0Ly8gXHRjb25zdCBzZWxlY3RVbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ1VMJyk7XHJcblx0XHRcdFx0Ly8gXHRzZWxlY3RVbC5jbGFzc05hbWUgPSAnZHJvcGRvd25fX2l0ZW0gZHJvcGRvd25fX3NlbGVjdCc7XHJcblxyXG5cdFx0XHRcdC8vIFx0Wy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50b29sc19fc29ydC1jYXInKV0uZm9yRWFjaCgoZiwgaW5kZXgpID0+IHtcclxuXHRcdFx0XHQvLyBcdFx0Y29uc3Qgc2VsZWN0TGkgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdMSScpO1xyXG5cdFx0XHRcdC8vIFx0XHRzZWxlY3RVbC5hcHBlbmRDaGlsZChzZWxlY3RMaSk7XHJcblx0XHRcdFx0Ly8gXHRcdHNlbGVjdExpLmlubmVySFRNTCArPSBmLmNoaWxkcmVuWzBdLnRleHRDb250ZW50O1xyXG5cdFx0XHRcdC8vIFx0fSk7XHJcblx0XHRcdFx0Ly8gXHRkcm9wbGlzdC5hcHBlbmRDaGlsZChzZWxlY3RVbCk7XHJcblxyXG5cdFx0XHRcdC8vIH0pXHJcblx0XHRcdFx0Ly8gLnRoZW4oKCkgPT4ge1xyXG5cdFx0XHRcdC8vIFx0Y29uc3QgZWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ01BSU4nKTtcclxuXHRcdFx0XHQvLyBcdGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoZXZlbnQpID0+IHtcclxuXHJcblx0XHRcdFx0Ly8gXHRcdGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5qc19fY3JlYXRlRHJvcExpc3QgVUwnKS5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJyk7XHJcblx0XHRcdFx0Ly8gXHRcdGlmIChldmVudC50YXJnZXQubWF0Y2hlcygnLmRyb3Bkb3duX19sYWJlbCcpKSB7XHJcblx0XHRcdFx0Ly8gXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKSAmJiBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHRcdFx0XHQvLyBcdFx0fVxyXG5cdFx0XHRcdC8vIFx0XHRpZiAoZXZlbnQudGFyZ2V0Lm1hdGNoZXMoJy5qc19fY3JlYXRlRHJvcExpc3QgSU5QVVQnKSkge1xyXG5cdFx0XHRcdC8vIFx0XHRcdGV2ZW50LnRhcmdldC5wYXJlbnRFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ1VMJykuY2xhc3NMaXN0LmFkZCgnb3BlbicpO1xyXG5cdFx0XHRcdC8vIFx0XHR9XHJcblx0XHRcdFx0Ly8gXHRcdGlmIChldmVudC50YXJnZXQubm9kZU5hbWUgPT09ICdMSScpIHtcclxuXHRcdFx0XHQvLyBcdFx0XHRjb25zdCBncmFiVGV4dCA9IGV2ZW50LnRhcmdldC50ZXh0Q29udGVudDtcclxuXHRcdFx0XHQvLyBcdFx0XHRjb25zdCBwYXN0ZUluU2VsZWN0b3IgPSBldmVudC50YXJnZXQucGFyZW50RWxlbWVudC5wYXJlbnRFbGVtZW50O1xyXG5cdFx0XHRcdC8vIFx0XHRcdHBhc3RlSW5TZWxlY3Rvci5xdWVyeVNlbGVjdG9yKCdJTlBVVCcpLnZhbHVlID0gZ3JhYlRleHQgfHwgJyc7XHJcblx0XHRcdFx0Ly8gXHRcdH1cclxuXHJcblx0XHRcdFx0Ly8gXHR9KTtcclxuXHRcdFx0XHQvLyB9KVxyXG5cdFx0XHRcdC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhgJHt0aGlzLmVycm9yVGV4dH06IFxcbiAtLT4gJHtlcnJvci5tZXNzYWdlfWApKTtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkTW92ZVNjcm9sbFVQIHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdHRoaXMuY291bnQgPSBOdW1iZXIoTWF0aC5hYnMoaW5pdC5zcGVlZCkpIHx8IDg7XHJcblx0XHR0aGlzLnNwZWVkID0gKHRoaXMuY291bnQgPD0gMjApID8gdGhpcy5jb3VudCA6IDg7XHJcblx0XHR0aGlzLm15UG9zID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG5cdFx0dGhpcy5nZXRTY3JvbGwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG5cdFx0dGhpcy5zcGVlZFNjcm9sbCA9IHRoaXMuc3BlZWQ7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IHNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLnNlbGVjdG9yfWApO1xyXG5cclxuXHRcdGlmIChzZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IGNsaWNrZWRBcnJvdyA9ICgpID0+IHtcclxuXHRcdFx0XHR0aGlzLmdldFNjcm9sbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3A7XHJcblx0XHRcdFx0aWYgKHRoaXMuZ2V0U2Nyb2xsID49IDEpIHtcclxuXHRcdFx0XHRcdHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoY2xpY2tlZEFycm93KTtcclxuXHRcdFx0XHRcdHdpbmRvdy5zY3JvbGxUbygwLCB0aGlzLmdldFNjcm9sbCAtIHRoaXMuZ2V0U2Nyb2xsIC8gdGhpcy5zcGVlZFNjcm9sbCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3Qgc2Nyb2xsZWREb3duID0gKCkgPT4ge1xyXG5cdFx0XHRcdHRoaXMubXlQb3MgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblx0XHRcdFx0KHRoaXMubXlQb3MgPj0gMTAwKSA/IHNlbGVjdG9yLmNsYXNzTGlzdC5hZGQoJ29wZW4nKSA6IHNlbGVjdG9yLmNsYXNzTGlzdC5yZW1vdmUoJ29wZW4nKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdHNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2xpY2tlZEFycm93KTtcclxuXHRcdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHNjcm9sbGVkRG93bik7XHJcblxyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRPcGVuTXlCdXJnZXIge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBpbml0LmJ1cmdlcjtcclxuXHRcdHRoaXMubmF2YmFyID0gaW5pdC5uYXZiYXI7XHJcblx0XHR0aGlzLmVycm9yVGV4dCA9ICfwn5KAIE91Y2gsIGRhdGFiYXNlIGVycm9yLi4uJztcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3Qgc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuc2VsZWN0b3J9YCk7XHJcblx0XHRpZiAoc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblxyXG5cdFx0XHRjb25zdCBvcGVuQnVyZ2VyID0gKCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IG5hdiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5uYXZiYXJ9YCk7XHJcblx0XHRcdFx0bmF2LnF1ZXJ5U2VsZWN0b3IoJ0RJVicpLmNsYXNzTGlzdC50b2dnbGUoJ29wZW4nKTtcclxuXHRcdFx0XHRzZWxlY3Rvci5jbGFzc0xpc3QudG9nZ2xlKCdvcGVuJyk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRzZWxlY3Rvci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IG9wZW5CdXJnZXIoKSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5QkE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQWZBO0FBQUE7QUFBQTtBQW9CQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFsSkE7QUFtSkE7QUFBQTtBQUNBO0FBQ0E7QUFyTEE7QUFBQTtBQUFBO0FBaUJBO0FBQ0E7QUFsQkE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBbkNBO0FBQUE7QUFBQTtBQVVBO0FBQUE7QUFWQTtBQUNBO0FBREE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBeEJBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFUQTtBQUNBO0FBREE7QUFBQTs7OztBIiwic291cmNlUm9vdCI6IiJ9