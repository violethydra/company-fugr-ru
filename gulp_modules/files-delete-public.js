/* ==== IMPORT PARAMS ==== */
import del from 'del';
/* ==== ----- ==== */

const PATHS = {
	pub: 'public',
	devTemp: 'development\\tmp',
	devPlug: 'development\\components\\plugins'
};

module.exports = () =>
	() => del([
		`${__dirname}\\..\\${PATHS.pub}`,
		`${__dirname}\\..\\${PATHS.devTemp}`,
		`${__dirname}\\..\\${PATHS.devPlug}`
	], { read: false });


