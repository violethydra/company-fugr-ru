// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddOpenMyBurger from './modules/openMyBurger';
import AddGeneratorTable from './modules/AddGeneratorTable';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	new AddMoveScrollUP({
		selector: '.js__moveScrollUP',
		speed: 8
	}).run();
	
	new AddOpenMyBurger({
		burger: '.js__navHamburger',
		navbar: '.js__navHamburgerOpener'
	}).run();
	
	new AddGeneratorTable({
		selector: '.js__selectorTable',
		server: 'server32.json',
		serverBig: 'server1000.json'
	}).run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
 