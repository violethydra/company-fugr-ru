module.exports = class AddGeneratorTable {
	constructor(init) {
		this.selector = init.selector;

		this.server = init.server;
		this.serverBig = init.serverBig;

		this.sortUp = (a, b) => (b < a) ? -1 : (b > a) ? 1 : (b >= a) ? 0 : NaN;
		this.sortDown = (a, b) => (a < b) ? -1 : (a > b) ? 1 : (a >= b) ? 0 : NaN;

		this.mydata = '';
		this.tableLine = '';

		this.errorText = '💀 Ouch, database error...';
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const selector = document.querySelector(`${this.selector}`);
		if (selector) {
			this.constructor.info();

			const init = {
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				mode: 'cors',
				cache: 'default'
			};

			fetch(`./${this.server}`, init)
				.then((response) => {
					response.headers.get('Content-Type');
					return response.json();
				})
				.then((data) => {
					selector.innerHTML = '';

					const createElemUX = document.createElement('DIV');
					createElemUX.classList.add('myformcontent__header');

					this.mydata = Object.keys(data[0]).filter((item, index, array) => index < array.length - 2);

					this.mydata.map((headerName) => {
						createElemUX.innerHTML += `
						<div class="myformcontent__header-item">
							<span class="myformcontent__header-text">${headerName}</span>
							<span class="myformcontent__arrow--down">
								<svg role="picture-svg" class="glyphs__caret-down"><use xlink:href="#id-glyphs-caret-down"></use></svg>
							</span>
						</div>
					`;
						return selector.appendChild(createElemUX);
					});

					this.tableLine = Object.values(data.slice(0, 50));
					const tableLineSortID = this.tableLine.sort((a, b) => this.sortDown(a[this.mydata[1]], b[this.mydata[1]]));

					tableLineSortID.forEach((f) => {
						const createElemLine = document.createElement('DIV');
						createElemLine.className = 'myformcontent__line';

						const tableLineItem = Object.values(f).filter((item, index, array) => index < array.length - 2);

						tableLineItem.forEach((item) => {
							const createElemLineItem = document.createElement('DIV');
							createElemLineItem.className = 'myformcontent__line-item';
							createElemLineItem.innerHTML += item;
							createElemLine.appendChild(createElemLineItem);
							return false;
						});

						selector.appendChild(createElemLine);
					});

				})
				.then(() => {
					const clickHeader = (event) => {
						const _this = event.target;
						const index = [..._this.parentElement.children].indexOf(_this);

						if (_this.classList.contains('myformcontent__header')) return;
						if (_this.parentElement.querySelector('.active')) {
							_this.parentElement.querySelector('.active').classList.remove('active');
						}

						this.tableLine.sort((a, b) => this.sortDown(a[this.mydata[index]], b[this.mydata[index]])).reverse();
						console.log('this.tableLine', this.tableLine);

						this.tableLine.forEach((f, indexArray) => {

							console.log('sort:', f.id);
							const numb = String(f.id);
							console.log('numb', numb, typeof numb);
			
							const _temp = [...document.querySelectorAll('.myformcontent__line')].filter(item => item.firstChild.textContent === numb);
							console.log('_temp', _temp[0], typeof _temp[0]);

							document.querySelector('.myformcontent__header').after(_temp[0]);

						});

						_this.parentElement.children[index].classList.add('active');
					};
					selector.querySelector('.myformcontent__header').firstElementChild.classList.add('active');
					selector.querySelector('.myformcontent__header').addEventListener('click', clickHeader);

				})

				// const clickHeader = (event) => {
				// 	console.log(event.target);
				// 	[...document.querySelectorAll('.myformcontent__header-item')].forEach(f => f.classList.remove('active'));
				// 	if (event.target.matches('.myformcontent__header-item')) {
				// 		event.target.classList.add('active');
				// 	}

				// 	selector.querySelector('.myformcontent__header-item').classList.remove('active');
				// };

				// const myRemove = [this.sortCar, this.sortHeader];
				// myRemove.forEach(el => (el.textContent.includes(this.errorText) ? el.remove() : ''));

				// Object.values(data[0].column).map((i, index) => {
				// 	const creatDIV = document.createElement('DIV');
				// 	creatDIV.className = 'tools__sort-header';
				// 	creatDIV.innerHTML += i.title;
				// 	selector.querySelector('.tools__sort-title').appendChild(creatDIV);
				// 	return false;
				// });

				// Object.values(data.slice(1)).forEach((i, index) => {
				// 	const createElemUX = document.createElement('DIV');
				// 	createElemUX.className = 'tools__sort-car tools__sort-line';

				// 	Object.values(i.car).forEach((f, fIndex) => {
				// 		const createElemDIV = document.createElement('DIV');
				// 		createElemDIV.className = 'tools__sort-item';
				// 		createElemDIV.innerHTML += f;
				// 		createElemUX.appendChild(createElemDIV);
				// 		return false;
				// 	});

				// 	selector.appendChild(createElemUX);
				// });

				// .then(() => {
				// 	const droplist = document.querySelector('.js__createDropList');
				// 	const selectUl = document.createElement('UL');
				// 	selectUl.className = 'dropdown__item dropdown__select';

				// 	[...document.querySelectorAll('.tools__sort-car')].forEach((f, index) => {
				// 		const selectLi = document.createElement('LI');
				// 		selectUl.appendChild(selectLi);
				// 		selectLi.innerHTML += f.children[0].textContent;
				// 	});
				// 	droplist.appendChild(selectUl);

				// })
				// .then(() => {
				// 	const element = document.querySelector('MAIN');
				// 	element.addEventListener('click', (event) => {

				// 		document.querySelector('.js__createDropList UL').classList.remove('open');
				// 		if (event.target.matches('.dropdown__label')) {
				// 			event.preventDefault() && event.stopPropagation();
				// 		}
				// 		if (event.target.matches('.js__createDropList INPUT')) {
				// 			event.target.parentElement.querySelector('UL').classList.add('open');
				// 		}
				// 		if (event.target.nodeName === 'LI') {
				// 			const grabText = event.target.textContent;
				// 			const pasteInSelector = event.target.parentElement.parentElement;
				// 			pasteInSelector.querySelector('INPUT').value = grabText || '';
				// 		}

				// 	});
				// })
				.catch(error => console.log(`${this.errorText}: \n --> ${error.message}`));
		}
	}
};
