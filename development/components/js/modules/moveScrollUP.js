module.exports = class AddMoveScrollUP {
	constructor(init) {
		this.selector = init.selector;
		this.count = Number(Math.abs(init.speed)) || 8;
		this.speed = (this.count <= 20) ? this.count : 8;
		this.myPos = window.pageYOffset;
		this.getScroll = document.documentElement.scrollTop;
		this.speedScroll = this.speed;
	}

	static info() { console.log('MODULE:', this.name, true); }

	run() {
		const selector = document.querySelector(`${this.selector}`);

		if (selector) {
			this.constructor.info();

			const clickedArrow = () => {
				this.getScroll = document.documentElement.scrollTop;
				if (this.getScroll >= 1) {
					window.requestAnimationFrame(clickedArrow);
					window.scrollTo(0, this.getScroll - this.getScroll / this.speedScroll);
				}
			};

			const scrolledDown = () => {
				this.myPos = window.pageYOffset;
				(this.myPos >= 100) ? selector.classList.add('open') : selector.classList.remove('open');
			};

			selector.addEventListener('click', clickedArrow);
			window.addEventListener('scroll', scrolledDown);

		}
	}
};
